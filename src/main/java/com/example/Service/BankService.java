package com.example.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Model.Bank;
import com.example.Repo.BankRepo;

@Service
public class BankService {
	
	@Autowired
	BankRepo bankRepo;
	
	public List<Bank> getAll()
	{
		return bankRepo.findAll();
	}
	
	public Bank saveCustomer(Bank b)
	{
		return bankRepo.save(b);
	}
	
	public void deleteCustomer(long id)
	{
		bankRepo.deleteById(id);
	}
	
	public Bank getCustomer(long id)
	{
		return bankRepo.getById(id);
	}

}
