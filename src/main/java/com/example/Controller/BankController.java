package com.example.Controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.Model.Bank;
import com.example.Service.BankService;

@ComponentScan("com.example.Service")

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("customer/")
public class BankController {

	
	@Autowired
	BankService bankSer;
	
	Bank b;
	
	@RequestMapping("/getAll")
	@ResponseBody
	public List<Bank> getAll()
	{
		return bankSer.getAll();
	}
	
	
	@GetMapping("/getCustomer/{id}")
	@ResponseBody
	public Bank getCustomer(@PathVariable long id)
	{
		return bankSer.getCustomer(id);
	}
	
	
	@PostMapping(value = "/saveCustomer")
	public Bank addCustomer(@RequestBody Bank b)
	{
		//Bank b = new Bank(id, accno, bal, cname);
		return bankSer.saveCustomer(b);
	}
	
	@DeleteMapping("/deleteCustomer/{id}")
	public String deleteCustomer(@PathVariable long id)
	{
		bankSer.deleteCustomer(id);
		return "DeleteSuccessful";
	}
	
}
