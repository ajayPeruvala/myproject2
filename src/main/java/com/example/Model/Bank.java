package com.example.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Entity
@Table(name="BankCustomers")
public class Bank {

	@Id	
	private long id;
	private int accno;
	private int bal;
	private String cname;
	/**
	 * @param id
	 * @param accno
	 * @param bal
	 * @param cname
	 */
	
	public Bank() {}
	public Bank(long id, int accno, int bal, String cname) {
		super();
		this.id = id;
		this.accno = accno;
		this.bal = bal;
		this.cname = cname;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getAccno() {
		return accno;
	}
	public void setAccno(int accno) {
		this.accno = accno;
	}
	public int getBal() {
		return bal;
	}
	public void setBal(int bal) {
		this.bal = bal;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	
	//"[{\"No\":\"17\",\"Name\":\"Andrew\"},{\"No\":\"18\",\"Name\":\"Peter\"}, {\"No\":\"19\",\"Name\":\"Tom\"}]"
	@Override
	public String toString() {
		return "{\"id\":\'"+id+"',\"accno\":\'"+accno+"',\"bal\":\'"+bal+"',\"cname\":\'"+cname+"'}";
	}
	
	
	
}
